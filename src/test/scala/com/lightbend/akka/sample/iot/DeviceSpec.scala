package com.lightbend.akka.sample.iot

import java.util.concurrent.TimeUnit

import akka.actor.{ActorSystem, PoisonPill, Props}
import akka.testkit.{TestKit, TestProbe}
import com.lightbend.akka.sample.iot.Device.{ReadTemperature, RecordTemperature, RespondTemperature}
import com.lightbend.akka.sample.iot.DeviceGroup.{ReplyDeviceList, RequestDeviceList}
import com.lightbend.akka.sample.iot.DeviceManager.{DeviceRegistered, ReplyGroupSet, RequestGroupSet, RequestTrackDevice}
import jdk.nashorn.internal.ir.RuntimeNode.Request
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, Matchers, WordSpecLike}

import scala.concurrent.duration.FiniteDuration

class DeviceSpec(_system: ActorSystem)
  extends TestKit(_system)
    with Matchers
    with WordSpecLike
    with BeforeAndAfterAll {
  //#test-classes

  def this() = this(ActorSystem("iot-application"))

  override def afterAll: Unit = {
    shutdown(system)
  }

  "reply with empty reading if no temperature is known" in {
    val probe = TestProbe()
    val deviceActor = system.actorOf(Device.props("group1", "device1"), "firstDevice")
    deviceActor.tell(Device.ReadTemperature(42), probe.ref)
    val response = probe.expectMsgType[Device.RespondTemperature]
    response.requestId should === (42)
    response.temperatureReading should === (None)
  }

  "record temperature correctly" in {
    val probe = TestProbe()
    val deviceActor = system.actorOf(Device.props("group2", "device2"))

    deviceActor.tell(RecordTemperature(requestId = 1, 24.0), probe.ref)
    val responseRecord1 = probe.expectMsgType[Device.TemperatureRecorded]
    responseRecord1.requestId should === (1)

    deviceActor.tell(ReadTemperature(requestId = 2), probe.ref)
    val responseRead1 = probe.expectMsgType[Device.RespondTemperature]
    responseRead1.requestId should === (2)
    responseRead1.temperatureReading should === (Some(24.0))

    deviceActor.tell(RecordTemperature(requestId = 3, 31.3), probe.ref)
    val responseRecord2 = probe.expectMsgType[Device.TemperatureRecorded]
    responseRecord2.requestId should === (3)

    deviceActor.tell(ReadTemperature(requestId = 4), probe.ref)
    val responseRead2 = probe.expectMsgType[Device.RespondTemperature]
    responseRead2.requestId should === (4)
    responseRead2.temperatureReading should === (Some(31.3))
  }

  "reply to registrations requests" in {
    val probe = TestProbe()
    val device = system.actorOf(Device.props("group3", "device1"))

    device.tell(DeviceManager.RequestTrackDevice("group3", "device1"), probe.ref)
    val response = probe.expectMsg(DeviceManager.DeviceRegistered)

    device.tell(DeviceManager.RequestTrackDevice("wrongGroup", "device1"), probe.ref)
    val response2 = probe.expectNoMessage(FiniteDuration(500, TimeUnit.MILLISECONDS))

    device.tell(DeviceManager.RequestTrackDevice("group3", "wrongDevice"), probe.ref)
    val response3 = probe.expectNoMessage(FiniteDuration(500, TimeUnit.MILLISECONDS))
  }

  "refuse registering a device to an incorrect group" in {
    val probe = TestProbe()
    val groupActor = system.actorOf(DeviceGroup.props("group4"), "group4")

    groupActor.tell(RequestTrackDevice("group2", "device1"), probe.ref)
    val response = probe.expectNoMessage(FiniteDuration(500, TimeUnit.MILLISECONDS))
  }

  "be able to successfully register a device actor" in {
    val probe = TestProbe()
    val groupActor = system.actorOf(DeviceGroup.props("group5"), "group5")

    groupActor.tell(RequestTrackDevice("group5", "device1"), probe.ref)
    val response = probe.expectMsg(DeviceManager.DeviceRegistered)
    val deviceActor1 = probe.lastSender

    groupActor.tell(RequestTrackDevice("group5", "device2"), probe.ref)
    val response2 = probe.expectMsg(DeviceManager.DeviceRegistered)
    val deviceActor2 = probe.lastSender
    deviceActor1 should !== (deviceActor2)

    deviceActor1.tell(Device.RecordTemperature(1, 23.0), probe.ref)
    probe.expectMsg(Device.TemperatureRecorded(1))
    deviceActor2.tell(Device.RecordTemperature(2, 23.0), probe.ref)
    probe.expectMsg(Device.TemperatureRecorded(2))
  }

  "return same actor for requested deviceId" in {
    val probe = TestProbe()
    val groupActor = system.actorOf(DeviceGroup.props("group6"))

    groupActor.tell(RequestTrackDevice("group6", "device-1"), probe.ref)
    probe.expectMsg(DeviceManager.DeviceRegistered)
    val device1 = probe.lastSender

    groupActor.tell(RequestTrackDevice("group6", "device-1"), probe.ref)
    probe.expectMsg(DeviceManager.DeviceRegistered)
    val device2 = probe.lastSender

    device1 should === (device2)
  }

  "device list message should return a set of device ids" in {
    val probe = TestProbe()
    val groupActor = system.actorOf(DeviceGroup.props("group7"))

    groupActor.tell(RequestTrackDevice("group7", "device1"), probe.ref)
    probe.expectMsg(DeviceManager.DeviceRegistered)
    groupActor.tell(RequestTrackDevice("group7", "device2"), probe.ref)
    probe.expectMsg(DeviceManager.DeviceRegistered)

    groupActor.tell(RequestDeviceList(1), probe.ref)
    probe.expectMsg(ReplyDeviceList(1, Set("device1", "device2")))
  }

  "be able to list active devices after one shuts down" in {
    val probe = TestProbe()
    val groupId = "group7"
    val groupActor = system.actorOf(DeviceGroup.props(groupId))

    groupActor.tell(RequestTrackDevice(groupId, "device1"), probe.ref)
    probe.expectMsg(DeviceManager.DeviceRegistered)
    val toShutdown = probe.lastSender
    groupActor.tell(RequestTrackDevice(groupId, "device2"), probe.ref)
    probe.expectMsg(DeviceManager.DeviceRegistered)

    groupActor.tell(RequestDeviceList(1), probe.ref)
    probe.expectMsg(ReplyDeviceList(1, Set("device1", "device2")))

    probe.watch(toShutdown)
    toShutdown ! PoisonPill
    probe.expectTerminated(toShutdown)

    probe.awaitAssert {
      groupActor.tell(RequestDeviceList(2), probe.ref)
      probe.expectMsg(ReplyDeviceList(2, Set("device2")))
    }
  }

  "list groups after one is shut down" in {
    val probe = TestProbe()
    val groupId = "group8"
    val managerActor = system.actorOf(DeviceManager.props)

    managerActor.tell(RequestTrackDevice(groupId, "device1"), probe.ref)
    probe.expectMsg(DeviceManager.DeviceRegistered)
    managerActor.tell(RequestTrackDevice(groupId, "device2"), probe.ref)
    probe.expectMsg(DeviceManager.DeviceRegistered)

    managerActor.tell(RequestGroupSet(1), probe.ref)
    val response = probe.expectMsgType[ReplyGroupSet]
    response.requestId should === (1)
    val groups = response.groups
    groups.isEmpty shouldBe (false)
    groups.size shouldBe (1)
    val group = groups.head

    probe.watch(group)
    group ! PoisonPill
    probe.expectTerminated(group)

    probe.awaitAssert {
      managerActor.tell(RequestGroupSet(2), probe.ref)
      probe.expectMsg(ReplyGroupSet(2, Set()))
    }
  }
}
