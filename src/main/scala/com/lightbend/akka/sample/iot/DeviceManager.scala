package com.lightbend.akka.sample.iot

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Terminated}
import com.lightbend.akka.sample.iot.DeviceManager.{ReplyGroupSet, RequestGroupSet, RequestTrackDevice}

object DeviceManager {
  def props: Props = Props(new DeviceManager)

  final case class RequestTrackDevice(groupId: String, deviceId: String)
  case object DeviceRegistered

  final case class RequestGroupSet(requestId: Long)
  final case class ReplyGroupSet(requestId: Long, groups: Set[ActorRef])
}

class DeviceManager extends Actor with ActorLogging {

  var groupIdToActor = Map.empty[String, ActorRef]
  var actorToGroupId = Map.empty[ActorRef, String]

  override def preStart(): Unit = log.info("Device manager started")
  override def postStop(): Unit = log.info("Device manager stopped")

  override def receive: Receive = {
    case trackMsg @ RequestTrackDevice(groupId, _) =>
      groupIdToActor.get(groupId) match {
        case Some(groupActor) => groupActor forward trackMsg
        case None => {
          log.info(s"Creating actor for $groupId")
          val groupActor = context.actorOf(DeviceGroup.props(groupId), s"$groupId")
          context.watch(groupActor)
          groupIdToActor += groupId -> groupActor
          actorToGroupId += groupActor -> groupId
          groupActor forward trackMsg
        }
      }
    case RequestGroupSet(requestId) => sender() ! ReplyGroupSet(requestId, actorToGroupId.keySet)
    case Terminated(groupRef) => {
      val groupId = actorToGroupId(groupRef)
      log.info(s"Actor for group $groupId has been terminated")
      groupIdToActor -= groupId
      actorToGroupId -= groupRef
    }
  }
}
