package com.lightbend.akka.sample.iot

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}

import scala.io.StdIn

object IotSupervisor {
  def props: Props = Props(new IotSupervisor)
}

class IotSupervisor extends Actor with ActorLogging {
  override def preStart(): Unit = log.info("IoT Application started")
  override def postStop(): Unit = log.info("IoT Application stopped")

  override def receive: Receive = Actor.emptyBehavior
}

object IotApp {

  def main(args: Array[String]): Unit = {
    val system = ActorSystem("iot")

    try {
      val supervisor = system.actorOf(Props[IotSupervisor], "iot-supervisor")
      StdIn.readLine()
    } finally {
      system.terminate()
    }
  }

}


