package com.lightbend.akka.sample.iot

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Terminated}
import com.lightbend.akka.sample.iot.DeviceGroup.{ReplyDeviceList, RequestDeviceList}
import com.lightbend.akka.sample.iot.DeviceManager.RequestTrackDevice

object DeviceGroup {
  def props(groupId: String): Props = Props(new DeviceGroup(groupId))
  
  final case class RequestDeviceList(requestId: Long)
  final case class ReplyDeviceList(requestId: Long, ids: Set[String])

  final case class RequestAllTemperatures(requestId: Long)
  final case class RespondAllTemperatures(requestId: Long, temperatures: Map[String, TemperatureReading])

  sealed trait TemperatureReading
  final case class Temperature(value: Double) extends TemperatureReading
  case object TemperatureNotAvailable extends TemperatureReading
  case object DeviceNotAvailable extends TemperatureReading
  case object DeviceTimedOut extends TemperatureReading
}

class DeviceGroup(groupId: String) extends Actor with ActorLogging {

  override def preStart(): Unit = log.info(s"Device group {} started", groupId)
  override def postStop(): Unit = log.info(s"Device group {} stopped", groupId)

  var deviceIdToActor: Map[String, ActorRef]  = Map.empty[String, ActorRef]
  var actorToDeviceId: Map[ActorRef, String]  = Map.empty[ActorRef, String]


  override def receive: Receive = {
    case trackMsg @ RequestTrackDevice(`groupId`, _) =>
      deviceIdToActor.get(trackMsg.deviceId) match {
        case Some(actorRef) => actorRef forward trackMsg
        case None => {
          log.info(s"Creating device actor for {}-{}", this.groupId, trackMsg.deviceId)
          val actorName = s"${trackMsg.deviceId}"
          val deviceActor = context.actorOf(Device.props(this.groupId, trackMsg.deviceId), actorName)
          context.watch(deviceActor)
          deviceIdToActor += trackMsg.deviceId -> deviceActor
          actorToDeviceId += deviceActor -> trackMsg.deviceId
          deviceActor forward trackMsg
        }
      }
    case RequestTrackDevice(groupId, deviceId) =>
      log.warning(s"Ignoring TrackDevice request for group {}. This actor is responsible for device group {}",
        groupId, this.groupId)
    case RequestDeviceList(reqId) => {
      sender() ! ReplyDeviceList(reqId, deviceIdToActor.keySet)
    }
    case Terminated(deviceActor) => {
      val deviceId = actorToDeviceId(deviceActor)
      log.info(s"Actor for device $deviceId has been terminated")
      actorToDeviceId -= deviceActor
      deviceIdToActor -= deviceId
    }
  }
}
