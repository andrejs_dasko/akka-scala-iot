package com.lightbend.akka.sample.iot

import akka.actor.{Actor, ActorLogging, Props}
import com.lightbend.akka.sample.iot.DeviceManager.RequestTrackDevice

object Device {
  def props(groupId: String, deviceId: String): Props = Props(new Device(groupId, deviceId))

  final case class RecordTemperature(requestId: Long, value: Double)
  final case class TemperatureRecorded(requestId: Long)

  final case class ReadTemperature(requestId: Long)
  final case class RespondTemperature(requestId: Long, temperatureReading: Option[Double])
}

class Device(groupId: String, deviceId: String) extends Actor with ActorLogging {
  import Device._

  override def preStart(): Unit = log.info("Device actor {}-{} started", groupId, deviceId)
  override def postStop(): Unit = log.info("Device actor {}-{} stopped", groupId, deviceId)

  var lastTemperatureReading: Option[Double] = None

  def queryTemperature: Option[Double] = ???

  override def receive: Receive = {
    case RequestTrackDevice(`groupId`, `deviceId`) =>
      sender() ! DeviceManager.DeviceRegistered
    case RequestTrackDevice(groupId, deviceId) => {
      log.warning(s"Ignoring TrackDevice request for {}-{}. This actor is responsible for {}-{}",
        groupId, deviceId, this.groupId, this.deviceId)
    }
    case RecordTemperature(reqId, temp) => {
      lastTemperatureReading = Some(temp)
      sender() ! TemperatureRecorded(reqId)
    }
    case ReadTemperature(reqId) =>
      sender() ! RespondTemperature(reqId, lastTemperatureReading)
  }
}
