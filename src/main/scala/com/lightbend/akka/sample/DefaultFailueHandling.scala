package com.lightbend.akka.sample

import akka.actor.{Actor, ActorRef, ActorSystem, Props}

class SupervisingActor extends Actor {
  val child: ActorRef = context.actorOf(Props[SupervisedActor], "child")

  override def receive: Receive = {
    case "failChild" => child ! "fail"
  }
}

class SupervisedActor extends Actor {
  override def preStart(): Unit = println("Supervised actor started")
  override def postStop(): Unit = println("Supervised actor stopped")

  override def receive: Receive = {
    case "fail" => {
      println("I, supervised actor, failed...")
      throw new Exception("failed")
    }
  }
}

object DefaultFailueHandling extends App {
  val system = ActorSystem("failureHandling")

  val supervisor = system.actorOf(Props[SupervisingActor], "supervisor")

  supervisor ! "failChild"

  system.terminate()
}
